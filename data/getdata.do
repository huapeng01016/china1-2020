*! version 1.0.0  10aug2020
*
* get data files for the talk
*

cscript

local data_dir = "c:/talks/china1-2020/data/"
/* covid daily data from JHU */
foreach date in 07-30-2020 08-01-2020 08-05-2020 08-10-2020 {
copy "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_daily_reports/`date'.csv" "`data_dir'`date'.csv", replace 
}

/* geopanda US fips json */
copy "https://raw.githubusercontent.com/plotly/datasets/master/geojson-counties-fips.json" "`data_dir'geojson-counties-fips.json", replace
 

